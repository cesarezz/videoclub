package com.pfm.spring.service;

import java.util.List;

import com.pfm.spring.model.Copia;

public interface CopiaService {

	public List<Copia> listCopias();

    public Copia listCopiaById(Integer id);
    
    public Copia copiaAlquiladaById(Integer id);
    
    public Copia copiaDevueltaById(Integer id);

    public Copia removeCopia(Integer id);
    
    public void removeCopiasArray(List<Copia> list);

    public Copia addCopia(Copia c);
    
    public List<Copia> crearCopiasPelicula(Integer id_pelicula,Integer cantidad);

    public Copia updateCopia(Copia c);	
}
