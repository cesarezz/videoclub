package com.pfm.spring.service;

import java.util.List;

import com.pfm.spring.model.Pelicula;

public interface PeliculaService {

	public List<Pelicula> listPeliculas();

        public Pelicula listPeliculaById(Integer id);

        public List<Pelicula> listPeliculasByTitulo(String titulo);

        public Pelicula removePelicula(Integer id);

        public Pelicula addPelicula(Pelicula p);

        public Pelicula updatePelicula(Pelicula p);	
}
