package com.pfm.spring.service;

import java.util.List;

import com.pfm.spring.model.Prestamo;

public interface PrestamoService {

	public List<Prestamo> listPrestamos();

    public Prestamo listPrestamoById(Integer id);
    
    public List<Prestamo> listPrestamoByClienteId(String cliente_id);
    
    public List<Prestamo> listPrestamoByCopiaId(Integer copia_id);

    public Prestamo removePrestamo(Integer id);

    public Prestamo addPrestamo(Prestamo p);

    public Prestamo updatePrestamo(Prestamo p);	
    
    public Prestamo removeCopia(Prestamo p);
    
    
}

