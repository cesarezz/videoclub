package com.pfm.spring.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pfm.spring.dao.PrestamoDAO;
import com.pfm.spring.model.Prestamo;
import com.pfm.spring.service.PrestamoService;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class PrestamoServiceImpl implements PrestamoService {
	
    @Autowired        
	private PrestamoDAO prestamoDAO;

	public void setPrestamoDAO(PrestamoDAO prestamoDAO) {
		this.prestamoDAO = prestamoDAO;
	}

	@Transactional
	public List<Prestamo> listPrestamos() {
		return this.prestamoDAO.listPrestamos();
	}

    @Transactional
    public Prestamo listPrestamoById(Integer id) {
        return this.prestamoDAO.listPrestamoById(id);
    }
    
    @Transactional
    public List<Prestamo> listPrestamoByClienteId(String cliente_id) {
        return this.prestamoDAO.listPrestamoByClienteId(cliente_id);
    }
    
    @Transactional
    public List<Prestamo> listPrestamoByCopiaId(Integer copia_id) {
        return this.prestamoDAO.listPrestamoByCopiaId(copia_id);
    }
    
    @Transactional
    public Prestamo removePrestamo(Integer id) {
        return this.prestamoDAO.removePrestamo(id);
    }

    @Transactional
    public Prestamo addPrestamo(Prestamo p) {
        return this.prestamoDAO.addPrestamo(p);
    }

    @Transactional
    public Prestamo updatePrestamo(Prestamo p) {
        return this.prestamoDAO.updatePrestamo(p);
    }
    
    @Transactional
    public Prestamo removeCopia(Prestamo p){
    	return this.prestamoDAO.removeCopia(p);
    }

}