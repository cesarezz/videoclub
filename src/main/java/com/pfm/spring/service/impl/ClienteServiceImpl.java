package com.pfm.spring.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pfm.spring.dao.ClienteDAO;
import com.pfm.spring.model.Cliente;
import com.pfm.spring.service.ClienteService;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ClienteServiceImpl implements ClienteService {
	
    @Autowired        
	private ClienteDAO clienteDAO;

	public void setClienteDAO(ClienteDAO clienteDAO) {
		this.clienteDAO = clienteDAO;
	}

	@Transactional
	public List<Cliente> listClientes() {
		return this.clienteDAO.listClientes();
	}

    @Transactional
    public Cliente listClienteByDni(String dni) {
        return this.clienteDAO.listClienteByDni(dni);
    }

    @Transactional
    public List<Cliente> listClientesByNombre(String titulo) {
        return this.clienteDAO.listClientesByNombre(titulo);
    }
    
    @Transactional
    public Cliente removeCliente(String dni) {
        return this.clienteDAO.removeCliente(dni);
    }

    @Transactional
    public Cliente addCliente(Cliente p) {
        return this.clienteDAO.addCliente(p);
    }

    @Transactional
    public Cliente updateCliente(Cliente p) {
        return this.clienteDAO.updateCliente(p);
    }

}
