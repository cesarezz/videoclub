package com.pfm.spring.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pfm.spring.dao.CopiaDAO;
import com.pfm.spring.dao.PrestamoDAO;
import com.pfm.spring.model.Copia;
import com.pfm.spring.model.Prestamo;
import com.pfm.spring.service.CopiaService;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class CopiaServiceImpl implements CopiaService {
	
    @Autowired        
	private CopiaDAO copiaDAO;
    
    @Autowired        
	private PrestamoDAO prestamoDAO;

	public void setCopiaDAO(CopiaDAO copiaDAO) {
		this.copiaDAO = copiaDAO;
	}

	@Transactional
	public List<Copia> listCopias() {
		return this.copiaDAO.listCopias();
	}

    @Transactional
    public Copia listCopiaById(Integer id) {
        return this.copiaDAO.listCopiaById(id);
    }
    
    @Transactional
    public Copia copiaAlquiladaById(Integer id) {
        return this.copiaDAO.copiaAlquiladaById(id);
    }
    
    @Transactional
    public Copia copiaDevueltaById(Integer id) {
        return this.copiaDAO.copiaDevueltaById(id);
    }
    
    @Transactional
    public Copia removeCopia(Integer id) {
    	
    	List<Prestamo> prestamosCopia =  prestamoDAO.listPrestamoByCopiaId(id);
    	
    	if(prestamosCopia.size() > 0){
    		
    		for (Prestamo prestamo : prestamosCopia){
        		this.prestamoDAO.removeCopia(prestamo);
        	}
    		
    	}    	
        return this.copiaDAO.removeCopia(id);
    }
    
    @Transactional
    public void removeCopiasArray(List<Copia> list){
    	
    	for (Copia copia : list){
    		
    		List<Prestamo> prestamosCopia =  prestamoDAO.listPrestamoByCopiaId(copia.getId());
        	
        	if(prestamosCopia.size() > 0){
        		
        		for (Prestamo prestamo : prestamosCopia){
            		this.prestamoDAO.removeCopia(prestamo);
            	}
        		
        	}        		
    		
    		this.copiaDAO.removeCopia(copia.getId());
    	}
    	
    }

    @Transactional
    public Copia addCopia(Copia p) {
        return this.copiaDAO.addCopia(p);
    }
    
    @Transactional
    public List<Copia> crearCopiasPelicula(Integer id_pelicula,Integer cantidad) {
        
    	List<Copia> copias_creadas = new ArrayList<Copia>();
    	
    	for (int i = 0; i<cantidad; i++) {
    		
    		Copia copia = new Copia();
    		copia.setPelicula_id(id_pelicula);
    		copia.setAlquilada(false);
    		copias_creadas.add(this.copiaDAO.addCopia(copia));
    		
		}
    	
    	return copias_creadas;
    }    

    @Transactional
    public Copia updateCopia(Copia p) {
        return this.copiaDAO.updateCopia(p);
    }

}

