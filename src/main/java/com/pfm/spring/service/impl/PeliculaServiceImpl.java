package com.pfm.spring.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pfm.spring.dao.PeliculaDAO;
import com.pfm.spring.model.Pelicula;
import com.pfm.spring.service.PeliculaService;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class PeliculaServiceImpl implements PeliculaService {
	
    @Autowired        
	private PeliculaDAO peliculaDAO;

	public void setPeliculaDAO(PeliculaDAO peliculaDAO) {
		this.peliculaDAO = peliculaDAO;
	}

	@Transactional
	public List<Pelicula> listPeliculas() {
		return this.peliculaDAO.listPeliculas();
	}

    @Transactional
    public Pelicula listPeliculaById(Integer id) {
        return this.peliculaDAO.listPeliculaById(id);
    }

    @Transactional
    public List<Pelicula> listPeliculasByTitulo(String titulo) {
        return this.peliculaDAO.listPeliculasByTitulo(titulo);
    }
    
    @Transactional
    public Pelicula removePelicula(Integer id) {
        return this.peliculaDAO.removePelicula(id);
    }

    @Transactional
    public Pelicula addPelicula(Pelicula p) {
        return this.peliculaDAO.addPelicula(p);
    }

    @Transactional
    public Pelicula updatePelicula(Pelicula p) {    	
        return this.peliculaDAO.updatePelicula(p);
    }

}
