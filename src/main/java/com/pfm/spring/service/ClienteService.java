package com.pfm.spring.service;

import java.util.List;

import com.pfm.spring.model.Cliente;

public interface ClienteService {

	public List<Cliente> listClientes();

    public Cliente listClienteByDni(String dni);

    public List<Cliente> listClientesByNombre(String nombre);

    public Cliente removeCliente(String dni);

    public Cliente addCliente(Cliente p);

    public Cliente updateCliente(Cliente p);	
}
