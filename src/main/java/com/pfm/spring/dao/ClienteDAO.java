package com.pfm.spring.dao;

import com.pfm.spring.model.Cliente;

import java.util.List;
 

public interface ClienteDAO {

	public List<Cliente> listClientes();

    public Cliente listClienteByDni(String dni);

    public List<Cliente> listClientesByNombre(String titulo);

    public Cliente removeCliente(String dni);

    public Cliente addCliente(Cliente p);

    public Cliente updateCliente(Cliente p);

}