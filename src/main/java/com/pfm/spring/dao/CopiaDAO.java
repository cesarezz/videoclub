package com.pfm.spring.dao;

import com.pfm.spring.model.Copia;
import java.util.List;
 

public interface CopiaDAO {

	public List<Copia> listCopias();

    public Copia listCopiaById(Integer id);
    
    public Copia copiaAlquiladaById(Integer id);
    
    public Copia copiaDevueltaById(Integer id);

    public Copia removeCopia(Integer id);

    public Copia addCopia(Copia c);

    public Copia updateCopia(Copia c);
}
