package com.pfm.spring.dao;

import com.pfm.spring.model.Prestamo;

import java.util.List;
 

public interface PrestamoDAO {

	public List<Prestamo> listPrestamos();

    public Prestamo listPrestamoById(Integer id);
    
    public List<Prestamo> listPrestamoByClienteId(String cliente_id);
    
    public List<Prestamo> listPrestamoByCopiaId(Integer copia_id);

    public Prestamo removePrestamo(Integer id);

    public Prestamo addPrestamo(Prestamo p);

    public Prestamo updatePrestamo(Prestamo p);
    
    public Prestamo removeCopia(Prestamo p);
}

