package com.pfm.spring.dao;

import com.pfm.spring.model.Pelicula;
import java.util.List;
 

public interface PeliculaDAO {

	public List<Pelicula> listPeliculas();

        public Pelicula listPeliculaById(Integer id);

        public List<Pelicula> listPeliculasByTitulo(String titulo);

        public Pelicula removePelicula(Integer id);

        public Pelicula addPelicula(Pelicula p);

        public Pelicula updatePelicula(Pelicula p);

}
