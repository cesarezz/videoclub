package com.pfm.spring.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.pfm.spring.dao.PrestamoDAO;
import com.pfm.spring.model.Prestamo;

import org.hibernate.Query;

@Repository
public class PrestamoDAOImpl extends GenericDAOImpl implements PrestamoDAO  {
	
	private static final Logger logger = LoggerFactory.getLogger(PrestamoDAOImpl.class);
        
	@SuppressWarnings("unchecked")
	public List<Prestamo> listPrestamos() {
            Session session = this.getSessionFactory().getCurrentSession();
            List<Prestamo> prestamoList = session.createQuery("from Prestamo").list();
            for(Prestamo p : prestamoList){
                    logger.info("Prestamo List::"+p);
            }
            return prestamoList;
	}

    public Prestamo listPrestamoById(Integer id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Prestamo prestamo =  (Prestamo) session.get(Prestamo.class, id);            
        logger.info("Prestamo List::"+prestamo);            
        return prestamo;                        
    }  
    
    public List<Prestamo> listPrestamoByClienteId(String cliente_id) {
    	Session session = this.getSessionFactory().getCurrentSession();
        String sql = "FROM Prestamo p WHERE p.cliente_id LIKE :cliente_id";
        Query query = session.createQuery(sql);
        query.setParameter("cliente_id", "%"+cliente_id+"%");
        List<Prestamo> prestamoList = query.list(); 
        logger.info("Prestamo List Cliente Id::"+ prestamoList);            
        return prestamoList;                       
    }  
    
    public List<Prestamo> listPrestamoByCopiaId(Integer copia_id) {
    	Session session = this.getSessionFactory().getCurrentSession();
        String sql = "FROM Prestamo p WHERE p.copia_id =:copia_id";
        Query query = session.createQuery(sql);
        query.setParameter("copia_id", copia_id);
        List<Prestamo> prestamoList = query.list(); 
        logger.info("Prestamo List Copia Id::"+ prestamoList);            
        return prestamoList;                       
    }  
       
	public Prestamo removePrestamo(Integer id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Prestamo c = (Prestamo) session.get(Prestamo.class, id);
		if(c != null){
			session.delete(c);
		}
		logger.info("Prestamo deleted successfully, Prestamo details=" + c);
                
                return c;
	}
        
    public Prestamo addPrestamo(Prestamo p) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.persist(p);
        logger.info("Prestamo add" + p);
        return p;
    }
        
    public Prestamo updatePrestamo(Prestamo p) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.update(p);
        logger.info("Prestamo update" + p);
        return p;
    }
    
    public Prestamo removeCopia(Prestamo p){
    	
    	Session session = this.getSessionFactory().getCurrentSession();
    	p.setCopia_id(null);
    	session.update(p);
        logger.info("Prestamo sin copia" + p);
        return p;
        
    }

}
