package com.pfm.spring.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.pfm.spring.dao.ClienteDAO;
import com.pfm.spring.model.Cliente;

import org.hibernate.Query;

@Repository
public class ClienteDAOImpl extends GenericDAOImpl implements ClienteDAO  {
	
	private static final Logger logger = LoggerFactory.getLogger(ClienteDAOImpl.class);
        
	@SuppressWarnings("unchecked")
	public List<Cliente> listClientes() {
            Session session = this.getSessionFactory().getCurrentSession();
            List<Cliente> clientesList = session.createQuery("from Cliente").list();
            for(Cliente p : clientesList){
                    logger.info("Cliente List::"+p);
            }
            return clientesList;
	}

    public Cliente listClienteByDni(String dni) {
        Session session = this.getSessionFactory().getCurrentSession();
        Cliente cliente =  (Cliente) session.get(Cliente.class, dni);            
        logger.info("Cliente List::"+cliente);            
        return cliente;                        
    }

    @SuppressWarnings("unchecked")
    public List<Cliente> listClientesByNombre(String nombre) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sql = "FROM Cliente c WHERE c.nombre LIKE :nombre";
        Query query = session.createQuery(sql);
        query.setParameter("nombre", "%"+nombre+"%");
        List<Cliente> clientesList = query.list(); 
        logger.info("Cliente List::"+ clientesList);            
        return clientesList;  
    }
        
       
	public Cliente removeCliente(String dni) {
		Session session = this.getSessionFactory().getCurrentSession();
		Cliente c = (Cliente) session.get(Cliente.class, dni);
		if(c != null){
			session.delete(c);
		}
		logger.info("Cliente deleted successfully, Cliente details=" + c);
                
        return c;
	}

        
    public Cliente addCliente(Cliente p) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.persist(p);
        logger.info("Person add" + p);
        return p;
    }

        
    public Cliente updateCliente(Cliente p) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.update(p);
        logger.info("Cliente update" + p);
        return p;
    }

}
