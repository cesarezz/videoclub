package com.pfm.spring.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.pfm.spring.dao.PeliculaDAO;
import com.pfm.spring.model.Pelicula;

import org.hibernate.Query;

@Repository
public class PeliculaDAOImpl extends GenericDAOImpl implements PeliculaDAO  {
	
	private static final Logger logger = LoggerFactory.getLogger(PeliculaDAOImpl.class);
        
	@SuppressWarnings("unchecked")
	public List<Pelicula> listPeliculas() {
            Session session = this.getSessionFactory().getCurrentSession();
            List<Pelicula> peliculaList = session.createQuery("from Pelicula").list();
            for(Pelicula p : peliculaList){
                    logger.info("Pelicula List::"+p);
            }
            return peliculaList;
	}

    public Pelicula listPeliculaById(Integer id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Pelicula pelicula =  (Pelicula) session.get(Pelicula.class, id);            
        logger.info("Pelicula List::"+pelicula);            
        return pelicula;                        
    }

    @SuppressWarnings("unchecked")
    public List<Pelicula> listPeliculasByTitulo(String titulo) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sql = "FROM Pelicula p WHERE p.titulo LIKE :titulo";
        Query query = session.createQuery(sql);
        query.setParameter("titulo", "%"+titulo+"%");
        List<Pelicula> peliculaList = query.list(); 
        logger.info("Pelicula List::"+ peliculaList);            
        return peliculaList;  
    }
        
       
	public Pelicula removePelicula(Integer id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Pelicula p = (Pelicula) session.get(Pelicula.class, id);
		if(p != null){
			session.delete(p);
		}
		logger.info("Person deleted successfully, person details=" + p);
                
        return p;
	}

        
    public Pelicula addPelicula(Pelicula p) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.persist(p);
        logger.info("Person add" + p);
        return p;
    }

        
    public Pelicula updatePelicula(Pelicula p) {  
    	
        Session session = this.getSessionFactory().getCurrentSession();        
        session.update(p);
        logger.info("Person update" + p);
        return p;
    }

}
