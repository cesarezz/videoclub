package com.pfm.spring.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.pfm.spring.dao.CopiaDAO;
import com.pfm.spring.model.Copia;

@Repository
public class CopiaDAOImpl extends GenericDAOImpl implements CopiaDAO  {
	
	private static final Logger logger = LoggerFactory.getLogger(CopiaDAOImpl.class);
        
	@SuppressWarnings("unchecked")
	public List<Copia> listCopias() {
            Session session = this.getSessionFactory().getCurrentSession();
            List<Copia> copiaList = session.createQuery("from Copia").list();
            for(Copia p : copiaList){
                    logger.info("Copia List::"+p);
            }
            return copiaList;
	}

    public Copia listCopiaById(Integer id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Copia copia =  (Copia) session.get(Copia.class, id);            
        logger.info("Copia List::"+copia);            
        return copia;                        
    }  
    
    public Copia copiaAlquiladaById(Integer id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Copia copia =  (Copia) session.get(Copia.class, id);         
        copia.setAlquilada(true);
        session.update(copia);
        logger.info("Copia Update Alquilada::"+copia);            
        return copia;                        
    } 
    
    public Copia copiaDevueltaById(Integer id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Copia copia =  (Copia) session.get(Copia.class, id);         
        copia.setAlquilada(false);
        session.update(copia);
        logger.info("Copia Update Devuelta::"+copia);            
        return copia;                        
    } 
       
	public Copia removeCopia(Integer id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Copia c = (Copia) session.get(Copia.class, id);
		if(c != null){
			session.delete(c);
		}
		logger.info("Copia deleted successfully, copia details=" + c);
                
        return c;
	}
        
    public Copia addCopia(Copia c) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.persist(c);
        logger.info("Copia add" + c);
        return c;
    }

        
    public Copia updateCopia(Copia c) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.update(c);
        logger.info("Copia update" + c);
        return c;
    }

}
