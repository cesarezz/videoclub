package com.pfm.spring.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="PELICULA")
public class Pelicula {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
	private int id; 	
        
    @Column(name = "titulo")
	private String titulo; 	
  
    @Column(name = "director")
	private String director; 	

    @Column(name = "annio")
	private Integer annio;
        
    @Column(name = "url")
    private String url;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "pelicula_id", nullable=true)
	private Set<Copia> copia = new HashSet<Copia>();
	
	public Pelicula(){
        
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Integer getAnnio() {
		return annio;
	}

	public void setAnnio(Integer annio) {
		this.annio = annio;
	}
        
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public Set<Copia> getCopia() {
		return copia;
	}

	public void setCopia(Set<Copia> copia) {
		this.copia = copia;
	}	

	@Override
	public String toString() {
		return "Pelicula [id=" + id + ", titulo=" + titulo + ", director="
				+ director + ", annio=" + annio + ", url=" + url + ", copia="
				+ copia + "]";
	}

	
}
