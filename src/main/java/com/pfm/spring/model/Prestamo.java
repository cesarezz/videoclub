package com.pfm.spring.model;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PRESTAMO")
public class Prestamo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
	private int id; 	
	
	private Integer copia_id; 
	
	private String cliente_id; 
	
	private Date recogida; 
	
	private Date devolucion;
	
	private String estado;

	public Prestamo(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getCopia_id() {
		return copia_id;
	}

	public void setCopia_id(Integer copia_id) {
		this.copia_id = copia_id;
	}

	public String getCliente_id() {
		return cliente_id;
	}

	public void setCliente_id(String cliente_id) {
		this.cliente_id = cliente_id;
	}

	public Date getRecogida() {
		return recogida;
	}

	public void setRecogida(Date recogida) {
		this.recogida = recogida;
	}

	public Date getDevolucion() {
		return devolucion;
	}

	public void setDevolucion(Date devolucion) {
		this.devolucion = devolucion;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Prestamo [id=" + id + ", copia_id=" + copia_id
				+ ", cliente_id=" + cliente_id + ", recogida=" + recogida
				+ ", devolucion=" + devolucion + ", estado=" + estado + "]";
	}		

}
