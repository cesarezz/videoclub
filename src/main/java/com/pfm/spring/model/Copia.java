package com.pfm.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author pankaj
 *
 */
@Entity
@Table(name="COPIA")
public class Copia {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
	private int id; 	
	
	@Column(name = "pelicula_id")
	private int pelicula_id;
	
	@Column(name = "alquilada")
	private boolean alquilada; 
	
	public Copia(){
        
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPelicula_id() {
		return pelicula_id;
	}

	public void setPelicula_id(int pelicula_id) {
		this.pelicula_id = pelicula_id;
	}

	public boolean isAlquilada() {
		return alquilada;
	}

	public void setAlquilada(boolean alquilada) {
		this.alquilada = alquilada;
	}

	@Override
	public String toString() {
		return "Copia [id=" + id + ", pelicula_id=" + pelicula_id
				+ ", alquilada=" + alquilada + "]";
	}

}
