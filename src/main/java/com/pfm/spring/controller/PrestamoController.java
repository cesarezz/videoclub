package com.pfm.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pfm.spring.model.Prestamo;
import com.pfm.spring.service.PrestamoService;

@RestController
@RequestMapping("/prestamos")
public class PrestamoController {
	
    @Autowired(required=true)
	private PrestamoService prestamoService;
	
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json" )
	public List<Prestamo> listPrestamos() {
		
		List<Prestamo> prestamos = prestamoService.listPrestamos();
		
		return prestamos;
	}
        
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json" )
	public Prestamo listPrestamoById(@PathVariable Integer id) {
                
    	Prestamo prestamo = prestamoService.listPrestamoById(id);
		
		return prestamo;
	}
    
    @RequestMapping(value = "/cliente/{dni}", method = RequestMethod.GET, produces = "application/json" )
   	public List<Prestamo> listPrestamoByClienteId(@PathVariable String dni) {
                   
    	List<Prestamo> prestamos = prestamoService.listPrestamoByClienteId(dni);
   		
   		return prestamos;
   	}
    
    @RequestMapping(value = "/copia/{id}", method = RequestMethod.GET, produces = "application/json" )
   	public List<Prestamo> listPrestamoByCopiaId(@PathVariable int id) {
                   
    	List<Prestamo> prestamo = prestamoService.listPrestamoByCopiaId(id);
   		
   		return prestamo;
   	}
        
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json" )
	public Prestamo addPrestamo(@RequestBody Prestamo p) {
                
    	Prestamo prestamo = prestamoService.addPrestamo(p);
		
		return prestamo;
	}
        
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = "application/json" )
	public Prestamo updatePrestamo(@RequestBody Prestamo p) {
                
    	Prestamo prestamo = prestamoService.updatePrestamo(p);
		
		return prestamo;
	}
        
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE, produces = "application/json" )
	public Prestamo removePrestamo(@PathVariable Integer id) {
                
    	Prestamo prestamo = prestamoService.removePrestamo(id);
		
		return prestamo;
	}
}
