package com.pfm.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pfm.spring.model.Copia;
import com.pfm.spring.model.Pelicula;
import com.pfm.spring.service.PeliculaService;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/peliculas")
public class PeliculaController {
	
    @Autowired(required=true)
	private PeliculaService peliculaService;
	
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json" )
	public List<Pelicula> listPeliculas() {
		
		List<Pelicula> peliculas = peliculaService.listPeliculas();
		
		return peliculas;
	}
        
        
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json" )
	public Pelicula listPeliculasById(@PathVariable Integer id) {
                
        Pelicula pelicula = peliculaService.listPeliculaById(id);
		
		return pelicula;
	}
        
    @RequestMapping(value = "/titulo/{titulo}", method = RequestMethod.GET, produces = "application/json" )
	public List<Pelicula> listPeliculasByTitulo(@PathVariable String titulo) {
                
        List<Pelicula> peliculas = peliculaService.listPeliculasByTitulo(titulo);
		
		return peliculas;
	}	
        
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json" )
	public Pelicula addPelicula(@RequestBody Pelicula p) {
                
    	Pelicula pelicula = peliculaService.addPelicula(p);
		
		return pelicula;
	}
        
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = "application/json" )
	public Pelicula updatePelicula(@RequestBody Pelicula p) {
                
    	Pelicula peliculaActual = peliculaService.listPeliculaById(p.getId());
    	
    	Set<Copia> copia = peliculaActual.getCopia();
    	
    	p.setCopia(copia);
    	
    	Pelicula pelicula = peliculaService.updatePelicula(p);
		
		return pelicula;
	}
        
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE, produces = "application/json" )
	public Pelicula removePelicula(@PathVariable Integer id) {
                
        Pelicula pelicula = peliculaService.removePelicula(id);
		
		return pelicula;
	}
}
