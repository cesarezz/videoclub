package com.pfm.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pfm.spring.model.Cliente;
import com.pfm.spring.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {
	
    @Autowired(required=true)
	private ClienteService clienteService;
	
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json" )
	public List<Cliente> listClientes() {
		
		List<Cliente> clientes = clienteService.listClientes();
		
		return clientes;
	}
        
    @RequestMapping(value = "/{dni}", method = RequestMethod.GET, produces = "application/json" )
	public Cliente listClienteByDni(@PathVariable String dni) {
                
    	Cliente cliente = clienteService.listClienteByDni(dni);
		
		return cliente;
	}
    
    @RequestMapping(value = "/nombre/{nombre}", method = RequestMethod.GET, produces = "application/json" )
	public List<Cliente> listClientesByNombre(@PathVariable String nombre) {
                
        List<Cliente> clientes = clienteService.listClientesByNombre(nombre);
		
		return clientes;
	}	
        
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json" )
	public Cliente addCliente(@RequestBody Cliente p) {
                
    	Cliente cliente = clienteService.addCliente(p);
		
		return cliente;
	}
        
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = "application/json" )
	public Cliente updateCliente(@RequestBody Cliente p) {
                
    	Cliente cliente = clienteService.updateCliente(p);
		
		return cliente;
	}
        
    @RequestMapping(value = "/remove/{dni}", method = RequestMethod.DELETE, produces = "application/json" )
	public Cliente removeCliente(@PathVariable String dni) {
                
    	Cliente cliente = clienteService.removeCliente(dni);
		
		return cliente;
	}
}
