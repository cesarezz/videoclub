package com.pfm.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pfm.spring.model.Copia;
import com.pfm.spring.service.CopiaService;

@RestController
@RequestMapping("/copias")
public class CopiaController {
	
    @Autowired(required=true)
	private CopiaService copiaService;
	
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json" )
	public List<Copia> listCopias() {
		
		List<Copia> copias = copiaService.listCopias();
		
		return copias;
	}
        
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json" )
	public Copia listCopiaById(@PathVariable Integer id) {
                
    	Copia copia = copiaService.listCopiaById(id);
		
		return copia;
	}
    
    @RequestMapping(value = "/alquilada/{id}", method = RequestMethod.GET, produces = "application/json" )
	public Copia copiaAlquiladaById(@PathVariable Integer id) {
                
    	Copia copia = copiaService.copiaAlquiladaById(id);
		
		return copia;
	}
    
    @RequestMapping(value = "/devuelta/{id}", method = RequestMethod.GET, produces = "application/json" )
	public Copia copiaDevueltaById(@PathVariable Integer id) {
                
    	Copia copia = copiaService.copiaDevueltaById(id);
		
		return copia;
	}
        
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json" )
	public Copia addCopia(@RequestBody Copia p) {
                
    	Copia copia = copiaService.addCopia(p);
		
		return copia;
	}
    
    @RequestMapping(value = "/pelicula/{id_pelicula}/cantidad/{cantidad}", method = RequestMethod.GET, produces = "application/json" )
   	public List<Copia> crearCopiasPelicula(@PathVariable Integer id_pelicula,@PathVariable Integer cantidad) {
                   
    	List<Copia> copias = copiaService.crearCopiasPelicula(id_pelicula,cantidad);
   		
   		return copias;
   	}
        
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = "application/json" )
	public Copia updateCopia(@RequestBody Copia p) {
                
    	Copia copia = copiaService.updateCopia(p);
		
		return copia;
	}
        
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE, produces = "application/json" )
	public Copia removeCopia(@PathVariable Integer id) {
                
    	Copia copia = copiaService.removeCopia(id);
		
		return copia;
	}
    
    @RequestMapping(value = "/remove/array_copias", method = RequestMethod.POST, produces = "application/json" )
	public void removeCopiasArray(@RequestBody List<Copia> list) {
                
    	copiaService.removeCopiasArray(list);
		
	}
}